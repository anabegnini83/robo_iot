# coding: utf-8

#Bibliotecas Necessárias
import RPi.GPIO as GPIO
import paho.mqtt.client as mqtt
from unicodedata import normalize
from carwalk import Carwalk
import time

#definicoes: 
Broker = "test.mosquitto.org"
topic = "/trabalho/info/industrial/robo/automo/industrial/teste/"
topic_subs = "/trabalho/info/industrial/robo/automo/industrial/"
PortaBroker = 1883

def on_connect(client, userdata, flags, rc):
    print("Connected!")
    client.subscribe(topic_subs)
 
#Callback - mensagem recebida do broker
def on_message(client, userdata, msg):

    Msg = str(msg.payload.decode("utf-8"))
    Msg = normalize('NFKD', Msg).encode('ASCII', 'ignore').decode('ASCII')

    print(f"Executando a acao: {Msg}") 
    
    if Msg == "frente":
        robo.frente()
    elif Msg == "pare":
        robo.pare()
    elif Msg == "direita":
        robo.direita()
    elif Msg == "esquerda":
        robo.esquerda()
    elif Msg == "re":
        robo.re()

def mqtt_client_connect():
    print("Connecting to Broker...")
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(Broker, PortaBroker)
    client.loop_start()

robo = Carwalk()
client = mqtt.Client("RPi")
mqtt_client_connect()

try:
  while True:
        client.publish(topic, "hello")
        time.sleep(2)
   

finally:
  GPIO.cleanup()