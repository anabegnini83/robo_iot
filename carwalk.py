# coding: utf-8

import RPi.GPIO as GPIO
from time import sleep

class Carwalk (object):

    def __init__(self):
        self.setup()

    def setup(self):
        GPIO.setmode(GPIO.BCM)

        #17declarando as saidas do motor A
        self.enableA = 18
        self.in1A = 27
        self.in2A = 22

        #declarando as saidas d motor B
        self.enableB = 19
        self.in1B = 5
        self.in2B = 6

        #set up
        GPIO.setup(self.enableA, GPIO.OUT)
        GPIO.setup(self.enableB, GPIO.OUT)
        GPIO.setup(self.in1A, GPIO.OUT)
        GPIO.setup(self.in2A, GPIO.OUT)
        GPIO.setup(self.in1B, GPIO.OUT)
        GPIO.setup(self.in2B, GPIO.OUT)
        self.motorA_pwm = GPIO.PWM(self.enableA, 500)
        self.motorB_pwm = GPIO.PWM(self.enableB, 500)
        self.motorA_pwm.start(0)
        self.motorB_pwm.start(0)

        self.velocidade = 50
        self.menor_velocidade = 20
        self.tempoGiro = 0.5

    
    # função que faz o carrinho ir para frente
    def frente (self):
        
        print("Indo para frente")
        # para o motor A
        GPIO.output(self.in1A, False)
        GPIO.output(self.in2A, True)
        # para o motor B
        GPIO.output(self.in1B, True)
        GPIO.output(self.in2B, False)
        # velocidade dos motores        
        self.motorA_pwm.ChangeDutyCycle(50)
        self.motorB_pwm.ChangeDutyCycle(45)
        sleep(2)
        self.pare()

    # funçao que faz o carrinho ir para a direita
    def esquerda (self):
        print("Virando para a esquerda")
        # para o motor A
        GPIO.output(self.in1A, False)
        GPIO.output(self.in2A, True)
        # para o motor B
        GPIO.output(self.in1B, True)
        GPIO.output(self.in2B, False)
        # velocidade dos motores
        self.motorA_pwm.ChangeDutyCycle(self.menor_velocidade)
        self.motorB_pwm.ChangeDutyCycle(self.velocidade)
        sleep(self.tempoGiro)
        self.pare()

    # funçao que faz o carrinho ir para esquerda
    def direita (self):
        print("Indo para a direita")
        # para o motor A
        GPIO.output(self.in1A, False)
        GPIO.output(self.in2A, True)
        # para o motor B
        GPIO.output(self.in1B, True)
        GPIO.output(self.in2B, False)
        # velocidade dos motores
        self.motorA_pwm.ChangeDutyCycle(self.velocidade)
        self.motorB_pwm.ChangeDutyCycle(self.menor_velocidade)
        sleep(self.tempoGiro)
        self.pare()
        

    #função que faz o carrinho dar ré
    def re (self):
        print("Dando ré!!")
        # para o motor A
        GPIO.output(self.in1A, True)
        GPIO.output(self.in2A, False)
        # para o motor B
        GPIO.output(self.in1B, False)
        GPIO.output(self.in2B, True)
        # velocidade dos motores        
        self.motorA_pwm.ChangeDutyCycle(self.velocidade)
        self.motorB_pwm.ChangeDutyCycle(self.velocidade)
        sleep(2)
        self.pare()

    # função de parar
    def pare (self):
        print("Parandooooo")
        # para o motor A
        GPIO.output(self.in1A, False)
        GPIO.output(self.in2A, False)
        # para o motor B
        GPIO.output(self.in1B, False)
        GPIO.output(self.in2B, False)
        # velocidade dos motores
        self.motorA_pwm.ChangeDutyCycle(0)
        self.motorB_pwm.ChangeDutyCycle(0)